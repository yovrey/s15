console.log('Hello World!')

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let firstName = "Jovrey", lastName = "Billones", currentAge = 33;
	console.log("First Name: " + firstName);
	console.log("Last Name: " + lastName);
	console.log("Age: " + currentAge);
	
	let hobbies = ["Freediving","Fishing","Spearfishing", "Trecking", "Camping"];
	console.log("Hobbies: ");
	console.log(hobbies);

	let workAddress = {

			houseNumber: "30 Hacienda Salinas",
			street: "Salinas Extension",
			city: "Cebu City",
			state: "Cebu",

	}
	console.log("Work Address: ")
	console.log(workAddress)

	console.log("My full name is: " + firstName + " " + lastName);
	console.log("My current age is: " + currentAge)

	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	
	let isActive = false;
	let fullProfile = {

			userName: "fullstack_developer",
			fullName: firstName + lastName,
			age: currentAge,
			isActive: isActive,

	}


	console.log("My Friends are: ")
	console.log(friends);

	console.log("My Full Profile: ")
	console.log(fullProfile);


	bffName = "Bucky Barnes";
	console.log("My bestfriend is: " + bffName);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Leyte, Philippines";
	console.log("I was found frozen in: " + lastLocation);

